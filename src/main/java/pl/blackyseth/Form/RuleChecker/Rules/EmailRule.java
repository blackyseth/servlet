package pl.blackyseth.Form.RuleChecker.Rules;


import pl.blackyseth.Form.FormObject;
import pl.blackyseth.Form.RuleChecker.CheckResult;
import pl.blackyseth.Form.RuleChecker.ICanCheckRule;
import pl.blackyseth.Form.RuleChecker.RuleResult;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.Normalizer;

public class EmailRule extends HttpServlet implements ICanCheckRule<String>{
    private CheckResult result = null;
    private String name;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        checkRule(req.getParameter("email"));
        resp.getWriter().write(result.getMessage());
    }

    @Override
    public CheckResult checkRule(String name) {
        //TODO Real Mail check
        if(name==null){
            result = new CheckResult("<i class=\"material-icons bad\">indeterminate_check_box</i>", RuleResult.Error);
        }else if(name.equals("")){
            result = new CheckResult("<i class=\"material-icons bad\">indeterminate_check_box</i>", RuleResult.Error);
        }else {
            result = new CheckResult("<i class=\"material-icons good\">check_box</i>", RuleResult.Ok);
        }
        return result;
    }
}