package pl.blackyseth.Form.RuleChecker;

public interface ICanCheckRule<TEntity> {
    CheckResult checkRule(TEntity entity);
}
