package pl.blackyseth.Form;



import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.servlet.http.HttpServletRequest;

@Entity
public class FormObject {

    @Id
    @GeneratedValue
    private int id;
    private String name;
    private String surname;
    private String email;
    private String emailValidator;
    private String employer;
    private PlaceOfInform placeOfInform;
    private String otherplaceOfInform;
    private String job;
    @Transient
    private HttpServletRequest req;


    public FormObject() {
    }

    public void setReq(HttpServletRequest req){
        this.req = req;
        setName();
        setSurname();
        setEmail();
        setEmailValidator();
        setEmployer();
        setPlaceOfInform();
        setOtherplaceOfInform();
        setJob();
    }

    public String getName() {
        return name;
    }

    public void setName() {
        this.name = req.getParameter("name");
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname() {
        this.surname = req.getParameter("surname");
    }

    public String getEmail() {
        return email;
    }

    public void setEmail() {
        this.email = req.getParameter("email");
    }

    public String getEmployer() {
        return employer;
    }

    public void setEmployer() {
        this.employer = req.getParameter("employer");
    }

    public String getEmailValidator() {
        return emailValidator;
    }

    public void setEmailValidator() {
        this.emailValidator = req.getParameter("validateemail");
    }

    public String getPlaceOfInform() {
        return String.valueOf(placeOfInform);
    }

    public void setPlaceOfInform() {
        char poi = String.valueOf(req.getParameter("placeOfInform")).charAt(0);
        switch (poi){
            case '1':{this.placeOfInform = PlaceOfInform.Job;break;}
            case '2':{this.placeOfInform = PlaceOfInform.School;break;}
            case '3': {this.placeOfInform = PlaceOfInform.Facebook; break;}
            case '4': {this.placeOfInform = PlaceOfInform.Friends;break;}
            default: {this.placeOfInform = null;break;}
        }
    }

    public String getOtherplaceOfInform() {
        return otherplaceOfInform;
    }

    public void setOtherplaceOfInform() {
        if (this.placeOfInform==null) {
            this.otherplaceOfInform = req.getParameter("otherPlaceOfInform");
        }
    }

    public String getJob() {
        return job;
    }

    public void setJob() {
        this.job = req.getParameter("job");
    }
}
