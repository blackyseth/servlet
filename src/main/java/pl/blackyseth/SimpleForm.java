package pl.blackyseth;

import org.hibernate.Session;
import pl.blackyseth.Form.FormObject;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;

public class SimpleForm extends HttpServlet{

    private static int counter = 5;

    private boolean check = false;
    private FormObject formObject;
    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;

    @Override
    public void init() throws ServletException {
        this.entityManagerFactory = Persistence.createEntityManagerFactory("Users");
        this.entityManager = entityManagerFactory.createEntityManager();
    }

    public SimpleForm() {
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        FormObject formObject;

        if (areEmpptySpaces()) {
            if(session.getAttribute("licznik").equals(0)){
                session.setAttribute("licznik",1);
                counter--;
                formObject = new FormObject();
                formObject.setReq(req);
                entityManager.persist(formObject);
                session.setAttribute("counter",counter);
                resp.sendRedirect("result.jsp");
            }else {
                resp.sendRedirect("result.jsp");
            }
        } else {
            resp.sendRedirect("empty.jsp");
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);

        if (areEmpptySpaces()) {
            if (session==null){
                session = req.getSession(true);

                session.setAttribute("licznik",0);
                session.setAttribute("counter",counter);

                resp.sendRedirect("index.jsp");
            }
        } else {
            resp.sendRedirect("empty.jsp");
        }
    }

    private boolean areEmpptySpaces(){
        return counter != 0;
    }
}
