<%--
  Created by IntelliJ IDEA.
  User: jjayd
  Date: 02.12.2015
  Time: 14:05
  To change this template use File | Settings | File Templates.
--%>
<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="org.hibernate.Session" %>
<html>
  <head>
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="js/ajax.js"></script>
    <link href='http://fonts.googleapis.com/css?family=Bitter' rel='stylesheet' type='text/css'>
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet"/>
    <title>Java 4 US!</title>
    <style>
      input{
        display: block;
      }
    </style>
  </head>
  <body>
    <div class="form-style-10">
      <h1>Java 4 US!<span style="float: right;">Wolne miejsca:  ${counter}</span><span>Zapisz się na konferencję programistyczną!</span></h1>
      <form action="SimpleForm" method="post">
        <div class="section"><span>1</span>Imię i nazwisko</div>
        <div class="inner-wrap">
          <label><span id="imie"><i class="material-icons">check_box_outline_blank</i></span>Imię: <input type="text" id="name" name="name" /></label>
          <label><span id="nazwisko"><i class="material-icons">check_box_outline_blank</i></span>Nazwisko: <input type="text" id="surname" name="surname" /></label>
        </div>

        <div class="section"><span>2</span>Email</div>
        <div class="inner-wrap">
          <label><span id="emaile"><i class="material-icons">check_box_outline_blank</i></span>Email:  <input type="email" id="email" name="email" /></label>
          <label><span id="validateemail"><i class="material-icons">check_box_outline_blank</i></span>Potwierdź email:  <input type="email" id="emailv" name="validateemail" /></label>
        </div>

        <div class="section"><span>3</span>Dodatkowe Info</div>
        <div class="inner-wrap">
          <label><span id="employer"><i class="material-icons">check_box_outline_blank</i></span>Twój pracodawca: <input type="text" id="employerek" name="employer" /></label>
          <label><span id="conferenceinfo"><i class="material-icons">check_box_outline_blank</i></span>Skąd wiesz o konferencji:
        <select id="poi" name="placeOfInform">
            <option disabled selected style="display: none;"></option>
            <option value="1">
              Ogłoszenie w pracy
            </option>
            <option value="2">
              Ogłoszenie na uczelni
            </option>
            <option value="3">
              Facebook
            </option>
            <option value="4">
              Znajomi
            </option>
            <option value="other">
              Inne, jakie ?
            </option>
          </select></label>
          <label><span id="otherer"><i class="material-icons">check_box_outline_blank</i></span>Inne: <input type="text" id="other" name="otherPlaceOfInform" disabled="disabled"/> </label>
          <label><span id="jober"><i class="material-icons">check_box_outline_blank</i></span>Stanowisko:  <input type="text" id="job" name="job"/></label>
        </div>
        <div class="button-section">
          <input type="submit" value="Zapisz się!"/>
        </div>
      </form>
    </div>

  </body>
</html>
