<%--
  Created by IntelliJ IDEA.
  User: jjayd
  Date: 06.12.2015
  Time: 02:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head><link href="css/style.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
        rel="stylesheet"/>
  <title></title>
  <style>
    #lol{
      line-height: 32px;
      vertical-align: middle;
    }
    .ok{
      font-size: 20px;
    }
    .good{
      line-height: 0;
      display: inline;
      vertical-align: middle;
    }
  </style>
</head>
<body>
<div class="form-style-10">
  <h1>Java 4 US!<span style="float: right;">Wolne miejsca:  ${counter}</span><span>Zapisz się na konferencję programistyczną!</span></h1>

  <div class="section"><span><i class="material-icons ok">clear</i></span>  Nope</div>
  <div class="inner-wrap">
    <label><span id="lol"><i class="material-icons good bad">indeterminate_check_box</i>Brak wolnych miejsc :(</span></label>
  </div>
</div>
</body>
</html>

