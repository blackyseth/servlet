$(document).ready(function(){
    $('#name').blur(
        function(event){
            var name = $('#name').val();
            $.post('NameRule',{
                    name:name
                }, function(responseText){
                    $('#imie').html(responseText);
                });
        });
});
$(document).ready(function(){
    $('#surname').blur(
        function(event){
            var name = $('#surname').val();
            $.post('SurnameRule',{
                surname:name
            }, function(responseText){
                $('#nazwisko').html(responseText);
            });
        });
});
$(document).ready(function(){
    $('#email').blur(
        function(event){
            var name = $('#email').val();
            $.post('EmailRule',{
                email:name
            }, function(responseText){
                $('#emaile').html(responseText);
            });
        });
});
$(document).ready(function(){
    $('#emailv').blur(
        function(event){
            var name = $('#emailv').val();
            $.post('ValidateEmailRule',{
               validateemail:name
            }, function(responseText){
                $('#validateemail').html(responseText);
            });
        });
});
$(document).ready(function(){
    $('#employerek').blur(
        function(event){
            var name = $('#employerek').val();
            $.post('EmployerRule',{
                employer:name
            }, function(responseText){
                $('#employer').html(responseText);
            });
        });
});
$(document).ready(function(){
    $('#other').blur(
        function(event){
            var name = $('#other').val();
            $.post('OtherPlaceOfInformRule',{
                otherPlaceOfInform:name
            }, function(responseText){
                $('#otherer').html(responseText);
            });
        });
});
$(document).ready(function(){
    $('#job').blur(
        function(event){
            var name = $('#job').val();
            $.post('JobRule',{
                job:name
            }, function(responseText){
                $('#jober').html(responseText);
            });
        });
});
$(document).ready(function(){
    $('#poi').change(
        function(event){
            var name = $('#poi').val();
            if(name!="other"){
                $('#other').prop({disabled:true});
                $('#otherer').html("<i class=\"material-icons\">check_box_outline_blank</i>");
            $.post('PlaceOfInformRule',{
                placeOfInform:name
            }, function(responseText){
                $('#conferenceinfo').html(responseText);
            });}else{
                $('#other').prop({disabled:false});
            }
        });
});
